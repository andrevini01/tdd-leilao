package com.leilao;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class Leiloeiro {
    String nome;
    Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance() {
        if(!leilao.lances.isEmpty()) {
            return leilao.lances.get(0);
        }else{
            return new Lance();
        }
    }
}