package com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    public List<Lance> lances = new ArrayList<>();

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance){
        if (validarLance(lance)){
            lances.add(lance);
        }
        return lance;
    }

    public boolean validarLance(Lance lance){
        Leilao leilao = new Leilao();
        leilao.lances = lances;
        Leiloeiro leiloeiro = new Leiloeiro("Carlos", leilao);

        Lance maiorLance = leiloeiro.retornarMaiorLance();
        return lance.getValorDoLance() > maiorLance.getValorDoLance();
    }
}
