package com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class LeilaoTests {

    @Test
    public void testarLeilaoAdicionarNovoLance(){
        Leilao leilao = new Leilao();
        Usuario usuario = new Usuario(1, "Andre");
        Lance lance = new Lance(usuario, 1000);
        Assertions.assertEquals(leilao.adicionarNovoLance(lance),lance);
    }

    @Test
    public void testarLeilaoValidarLanceValido(){
        Leilao leilao = new Leilao();
        Usuario usuario = new Usuario(1, "Andre");
        Lance lance = new Lance(usuario, 1000);
        leilao.adicionarNovoLance(new Lance(usuario, 10));
        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarLeilaoValidarLancePrimeiroLance(){
        Leilao leilao = new Leilao();
        Usuario usuario = new Usuario(1, "Andre");
        Lance lance = new Lance(usuario, 1000);
        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarLeilaoValidarLanceInvalido(){
        Leilao leilao = new Leilao();
        Usuario usuario = new Usuario(1, "Andre");
        Lance lance = new Lance(usuario, 1000);
        Lance lanceInvalido = new Lance(usuario, 800);

        leilao.adicionarNovoLance(lance);

        Assertions.assertEquals(leilao.validarLance(lanceInvalido), false);
    }


}
