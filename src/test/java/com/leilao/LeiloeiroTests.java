package com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeiloeiroTests {
    @Test
    public void testarLeiloeiroRetornarMaiorLance(){
        Leilao leilao = new Leilao();
        Leiloeiro leiloeiro = new Leiloeiro("Carlos", leilao);
        Usuario usuario = new Usuario(1, "Andre");
        Lance lanceMaior = new Lance(usuario, 1000);
        Lance lanceMenor = new Lance(usuario, 120);

        leilao.adicionarNovoLance(lanceMaior);
        leilao.adicionarNovoLance(lanceMenor);

        Assertions.assertEquals(leiloeiro.retornarMaiorLance().getValorDoLance(), lanceMaior.getValorDoLance());
    }
}
